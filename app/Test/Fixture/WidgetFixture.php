<?php
/**
 * Widget Fixture
 */
class WidgetFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'biginteger', 'null' => false, 'default' => null, 'length' => 11, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100),
		'part_no' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 12),
		'quantity' => array('type' => 'integer', 'null' => true, 'default' => null),
		'indexes' => array(
			
		),
		'tableParameters' => array()
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'part_no' => 'Lorem ipsu',
			'quantity' => 1
		),
	);

}
